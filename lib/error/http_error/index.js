class HttpError extends Error {
    constructor(codeError, message) {
        super(message);
        this.message = message;
        this.codeError = codeError;
    }
}
exports.HttpError = HttpError;