"use strict";

const User = require('../models/user').User;

class Auth {
    static auth(name, pass) {
        if (name == null || pass == null) return new Error();

        return new Promise((resolve, reject) => {
            User.findOne({username: name}).then(value => {
                if (value == null) reject(new Error('User not found'));

                if (value.checkPassword(pass)) {
                    resolve(value);
                } else {
                    throw new Error('Password incorrect');
                }
            }).catch(err => {
                reject(err);
            });
        });
    }
}

exports.Auth = Auth;