const mongoose = require('mongoose');

global.gconfig = require('../config/config');

mongoose.connect(gconfig.mongoose.uri, {useNewUrlParser: true});

module.exports = mongoose;