"use strict";

const cookie = require('cookie');
const cookieParser = require('cookie-parser');
const SessionsStore = require('../lib/SessionsStore').SessionStore;
const User = require('../models/user').User;

class Socket {
    constructor(io) {
        if (io == null)
            throw new SocketError('Socket not include');

        this.io = io;

        /**
         * Auth in server
         */
        this.authorization(io);

        this.socket = null;

        /**
         * Connect with client
         */
        this.connection();
    }

    get events() {
        return {
            'message': 'messageSocket'
        }
    }

    connection() {
        let io = this.io;

        io.on('connect', socket => {
            /**
             * Register Action
             */
            this.registerSocketsEvent(socket);
        });
    }

    /**
     * Register function for action socket
     *
     */
    registerSocketsEvent(socket) {
        let events = this.events; //get events
        this.socket = socket;

        Object.keys(events).forEach( event => {
            let functionEvent = events[event];

            socket.on(event, (data, callback) => {
                this[functionEvent](event, data, callback);
            });
        });
    }

    /**
     * Function for authorization in WS
     * @param io object socket.IO
     */
    authorization() {
        let io = this.io;

        io.use((socket, next) => {

            let handshakeData = socket.request;
            handshakeData.cookies = cookie.parse(handshakeData.headers.cookie);

            let sidCookies = handshakeData.cookies[gconfig.session.key];
            let sid = cookieParser.signedCookie(sidCookies, gconfig.session.secret);

            let sessionsStore = new SessionsStore();

            sessionsStore.get(sid).then(val =>  {
                User.findById(val.user).then(user => {
                    socket.request.user = user;
                    next();
                });
            }).catch(err => {
                next(new SocketError('Error connect socket'));
            });
        });
    }
}

class SocketError extends Error {
    constructor(message) {
        super(message);
    }
}

exports.Socket = Socket;