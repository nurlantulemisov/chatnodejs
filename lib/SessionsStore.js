const session = require('express-session');
const mongoose = require('./mongoose');
const MongoStore = require('connect-mongo')(session);

class SessionsStore extends MongoStore{
    constructor () {
        super({mongooseConnection: mongoose.connection});
    }
}

exports.SessionStore = SessionsStore;