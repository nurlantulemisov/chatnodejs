const express = require('express');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const bodyParser = require('body-parser');

global.gconfig = require('./config/config');

const app = express();

class Server {
    constructor() {
        this.initDb();
        this.initMiddleware();
        this.initEngineView();
        this.initRouters();
        this.start();
    }

    start() {
        const server = app.listen(gconfig.port, function () {
            console.log('server running on port ' + gconfig.port);
        });

        const io = require('socket.io')(server);
        this.initSockets(io);
    }

    initMiddleware() {
        app.use(bodyParser());
        app.use(cookieParser());
        app.use(require('./middleware/loadUser'));
        app.use(require('./middleware/newRequest'));
        app.use(require('./middleware/loadMenu'));
    }

    initDb() {
        //session

        const SessionsStore = require('./lib/SessionsStore').SessionStore;

        app.use(session({
            secret: gconfig.session.secret,
            cookie: gconfig.cookie,
            key: gconfig.session.key,
            store: new SessionsStore()
        }));
    }

    initEngineView() {
        //ejs
        app.set('views', __dirname + "/public");
        app.set('view engine', 'ejs');
        //static
        app.use(express.static(__dirname + '/public'));
    }

    initRouters() {
        require('./routers')(app);
    }

    initSockets(io) {
        const SocketActions = require('./sockets/message').SocketActions;
        new SocketActions(io);
    }
}

new Server();
