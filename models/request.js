const   mongoose = require('../lib/mongoose'),
        Schema = mongoose.Schema;

let schema = new Schema({
    id_user_to: {
        type: String,
        required: true
    },
    id_friend_from: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

exports.Request = mongoose.model('Request', schema);