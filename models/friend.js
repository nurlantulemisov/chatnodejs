const   mongoose = require('../lib/mongoose'),
        Schema = mongoose.Schema;

const Request = require('./request').Request;
const User = require('./user').User;
const ObjectId = require('mongodb').ObjectID;

let schema = new Schema({
    id_user: {
        type: String
    },
    id_friend: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

/**
 * Список заявок в друзья
 * @param id_user
 * @returns {Promise<*>}
 */
schema.methods.newFriends = async function(id_user) {
    if (!id_user) return false;

    let idsRequest = await Request.find({id_friend_from: id_user});

    let users = [];

    for (let item in idsRequest) {
        let user = await this.findUserById(idsRequest[item].id_user_to);
        users.push(user);
    }

    return users;
};
/**
 * Список исходящих заявок
 * @param id_user
 * @returns {Promise<*>}
 *
 * Да дублирование, понимаю
 */
schema.methods.outFriends = async function(id_user) {
    if (!id_user) return false

    let model = mongoose.model('Friend', schema);

    let idsRequest = await Request.find({id_user_to: id_user});

    let users = [];

    for (let item in idsRequest) {
        let user = await this.findUserById(idsRequest[item].id_friend_from);
        users.push(user);
    }

    return users;
};

schema.methods.findFriends = async function(id_user) {
    if (!id_user) return false;

    let model = mongoose.model('Friend', schema);

    let users = await model.find().or([{id_user: id_user}, {id_friend: id_user}]);

    let returns = [];

    for (let item in users) {
        let user;

        if (users[item].id_user != id_user) {
            user = this.findUserById(users[item].id_user);
        }
        if (users[item].id_friend != id_user) {
            user = this.findUserById(users[item].id_friend);
        }
        returns.push(await user);
    }
    return returns;
};

/**
 *
 * @param id_user
 * @returns {Promise<void>}
 */
schema.methods.findUserById = async function(id_user) {
    let id = new ObjectId(id_user);

    return await User.findById(id);
};

exports.Friend = mongoose.model('Friend', schema);