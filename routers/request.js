const Router = require('../lib/Routers');
const Request = require('../models/request').Request;
const HttpError = require('../lib/error/http_error/index').HttpError;

class RequestRouter extends Router {
    get services() {
        return {
            'POST /add': 'addAction'
        };
    }

    addAction(req, res, next) {

        if (!req.user) next(new HttpError(422, 'user not in system'));

        let id = req.body.id_friend;
        if (id == null) next();

        let newRequest = new Request({
            id_user_to: req.user._id,
            id_friend_from: id
        });

        newRequest.save().then(val => {
            res.json({'message': 'Add'});
        }).catch(err => {
            next(err);
        });
    }
}

module.exports = RequestRouter;