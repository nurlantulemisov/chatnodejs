const Router = require('../lib/Routers');
const HttpError = require('../lib/error/http_error/index').HttpError;

class ChatRouter extends Router {
    get services() {
        return {
            '/': 'indexAction',
        };
    }

    indexAction(req, res, next) {
        if (!req.user) {
            this.app.set('error', 'Доступ закрыт');
            res.redirect('/');
        }

        let model = {
            title: "Чат", layout: false
        };
        res.render('chat', model);
    }

}

module.exports = ChatRouter;