const Router = require('../lib/Routers');
const User = require('../models/user').User;
const HttpError = require('../lib/error/http_error/index').HttpError;
const Auth = require('../lib/Auth').Auth;

class AuthRouter extends Router {
    get services() {
        return {
            'POST /logout': 'logoutAction',
            '/login': 'loginAction',
            'POST /auth': 'authAction'
        };
    }

    loginAction(req, res, next) {
        let error = this.app.get('error') !== undefined ? this.app.get('error') : '';
        let model = {
            title: "Авторизация", layout: false, menu: [
                {name: 'Главная', link: '/'}
            ],
            error: error
        };
        res.render('auth', model);
    }

    logoutAction(req, res, next) {
        req.session.destroy();
        res.json(1);
    }

    authAction(req, res, next) {
        let login = req.body.login;
        let pass = req.body.pass;
        let name = req.body.name;

        let that = this;

        // Здесь я тоже понимаю, что нельзя использовать
        // две реализации одного и того же

        Auth.auth(login, pass).then(user => {
            req.session.user = user._id;
            that.app.set('error', '');
            res.redirect('/profile');
        }).catch(err => {

            if (req.session.user) {
                req.session.destroy();
            }

            let user = that.createUser(login, pass, name);

            if (user == null) {
                next(new HttpError('user not created'))
            }
        });
    }

    /**
     * Я понимаю метод не совсем метод роутера, но все же
     *
     * @param login
     * @param pass
     * @param name
     * @returns {Promise<*>}
     */
    async createUser(login, pass, name = 'default name') {
        let user = new User({
            'name': name,
            'username': login,
            'password': pass
        });

        return await user.save();;
    }
}

module.exports = AuthRouter;