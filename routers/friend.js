const Router = require('../lib/Routers');
const Friend = require('../models/friend').Friend;
const Request = require('../models/request').Request;
const HttpError = require('../lib/error/http_error/index').HttpError;

class FriendRouter extends Router {
    get services() {
        return {
            '/': 'allAction',
            'POST /add': 'addAction',
            'POST /delete-request': 'deleteRequestAction',
            'POST /delete-friend': 'deleteFriendAction'
        };
    }

    allAction(req, res, next) {

        this.renderRequests(req.user._id).then(data => {
            let model = {
                title: "Заявки", layout: false,
                usersNew: data['inbox'],
                usersOut: data['outbox'],
                friends: data['friends']
            };
            res.render('friend', model);
        });
    }

    addAction(req, res, next) {
        let idFriend = req.body.id_friend;

        let friends = new Friend({
            id_user: req.user._id,
            id_friend: idFriend
        });

        friends.save().then(val => {
            Request.find().and([{id_user_to: idFriend, id_friend_from: req.user._id}]).then(r => {
                Request.deleteOne({ id: r._id }).then(v => {
                    res.json(v);
                });
            });
        });
    }

    deleteRequestAction(req, res, next) {
        let idFriend = req.body.id_friend;

        Request.find().and([{id_user_to: idFriend, id_friend_from: req.user._id}]).then(r => {
            Request.deleteOne({ id: r._id }).then(v => {
                res.json(v);
            });
        });
    }

    async deleteFriendAction(req, res, next) {
        let idFriend = req.body.id_friend;

        let user = await Friend.find().and([{id_user: idFriend, id_friend: req.user._id}]);
        await Friend.deleteOne({ id: user._id });

        user = await Friend.find().and([{id_friend: idFriend, id_user: req.user._id}]);
        await Friend.deleteOne({ id: user._id });

        res.json({ok: 1});
    }

    /**
     *
     * @param id_user
     * @returns {Promise<Array>}
     */
    async renderRequests(id_user) {
        let friends = new Friend();
        let data = [];

        data['inbox'] = await friends.newFriends(id_user);
        data['outbox'] = await friends.outFriends(id_user);
        data['friends'] = await friends.findFriends(id_user);

        return data;
    }

    async deleteFriend(idUser, idFriend) {

    }
}

module.exports = FriendRouter;