const Router = require('../lib/Routers');
const User = require('../models/user').User;
const HttpError = require('../lib/error/http_error/index').HttpError;

class SearchRouter extends Router {
    get services() {
        return {
            '/': 'searchAction',
            'POST /name': 'searchNameAction'
        };
    }

    searchAction(req, res, next) {
        let model = {
            title: "Поиск друзей", layout: false
        };
        res.render('search', model);
    }

    searchNameAction(req, res, next) {
        let searchText = req.body.search_text;
        User.find({name: { $regex: '.*' + searchText + '.*' }}).then(user => {
            res.json(user);
        });
    }
}

module.exports = SearchRouter;