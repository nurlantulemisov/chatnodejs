const HttpError = require('../lib/error/http_error').HttpError;
const UserRouters = require('./user');
const AuthRouters = require('./auth');
const ChatRouters = require('./chat');
const ProfileRouters = require('./profile');
const SearchRouters = require('./search');
const RequestRouters = require('./request');
const FriendRouters = require('./friend');


module.exports = app => {
    app.get('/', function (req, res) {

        if (req.user != null) {
            res.redirect('/profile');
        }
        res.redirect('/auth/login');
    });

    let userRouters     = new UserRouters('/users', app);
    let authRouters     = new AuthRouters('/auth', app);
    let chatRouters     = new ChatRouters('/chat', app);
    let profileRouters  = new ProfileRouters('/profile', app);
    let searchRouters   = new SearchRouters('/search', app);
    let requestRouters  = new RequestRouters('/request', app);
    let friendRouters   = new FriendRouters('/friend', app);


    app.use((err, req, res, next) => {
        if (err instanceof HttpError) {
            res.send(err.codeError, err.message);
        } else {
            console.log(err);
            res.statusCode = 500;
        }
    });
};
