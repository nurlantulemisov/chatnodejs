const Router = require('../lib/Routers');
const User = require('../models/user').User;
const HttpError = require('../lib/error/http_error/index').HttpError;
const ObjectId = require('mongodb').ObjectID;

class ProfileRouter extends Router {
    get services() {
        return {
            '/': 'profileAction',
            '/:id': 'userAction'
        };
    }

    profileAction(req, res, next) {
        let error = this.app.get('error') !== undefined ? this.app.get('error') : '';
        let model = {
            title: "Профиль", layout: false,
            error: error,
            users: {'name': req.user.name, 'username': req.user.username, 'created': req.user.created},
        };
        res.render('profile', model);
    }

    userAction(req, res, next) {
        let id;

        try {
            id = new ObjectId(req.params.id);
        } catch (e) {
            next(new HttpError(404, 'User not found'));
        }

        User.findById(id).then(value => {
            let data = {
                title: "Профиль " + value.name, layout: false,
                users: {'name': value.name, 'username': value.username, 'created': value.created},
            };
            res.render('profile', data);
        }).catch(err => {
            next(new HttpError(404, 'User not found'));
        });
    }
}

module.exports = ProfileRouter;