const User = require('../models/user').User;
const HttpError = require('../lib/error/http_error').HttpError;
const ObjectId = require('mongodb').ObjectID;
const Router = require('../lib/Routers');

class UserRouters extends Router {
    get services() {
        return {
            '/': 'usersAction',
            '/:id': 'userAction'
        };
    }

    usersAction(req, res, next) {
        User.find({}).then((value) => {
            let model = {
                title: 'Пользователи',
                layout: false,
                users: value,
                menu: [
                    {name: 'Главная', link: '/'},
                    {name: 'Выйти', link: '/auth/logout'},
                    {name: 'Войти', link: '/auth/login'}
                    ]
            };
            res.render('user', model);
        }).catch(err => {
            if (err) console.error(err);
        });
    }

    userAction(req, res, next) {
        let id;
        try {
            id = new ObjectId(req.params.id);
        } catch (e) {
            next(new HttpError(404, 'User not found'));
        }
        User.findById(id).then(value => {
            res.json(value);
        }).catch(err => {
            next(new HttpError(404, 'User not found'));
        });
    }
}

module.exports = UserRouters;
