const Friend = require('../models/friend').Friend;

module.exports = async (req, res, next) => {
    let users = [];

    if (req.user) {
        let friendMdl = new Friend();
        users = await friendMdl.newFriends(req.user._id);

    }

    res.locals.newRequests = users.length;
    next();
};