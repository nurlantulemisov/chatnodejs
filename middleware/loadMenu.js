module.exports = (req, res, next) => {
    let menu = {};
    if (req.user) {
        let newReq = res.locals.newRequests;
        menu = [
            {name: 'Profile', link: '/profile', type: 'fa-user', active: ''},
            {name: 'Friends', link: '/friend', type: 'fa-user', active: '', new_req: newReq},
            {name: 'Search', link: '/search', type: 'fa-search', active: ''},
            {name: 'Exit', link: 'javascript://', type: '', active: '', id: 'exit'}
            ];

        for (let item in menu) {
            if (req.url.indexOf(menu[item].link) + 1) {
                menu[item].active = 'active';
            }
        }
    }

    res.locals.menu = menu;
    next();
};