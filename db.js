const mongoose = require('./lib/mongoose');
//let async = require('async');
const User = require('./models/user').User;

mongoose.connection.on('open', () => {
    let db = mongoose.connection.db;
    console.log('open');
    db.dropDatabase((err) => {
        if (err) throw err;

        console.log('dropDatabase');
    });

    let admin = new User({
        'username': 'admin',
        'password': '12345'
    });
    let root = new User({
        'username': 'root',
        'password': '12345'
    });
    let nurlan = new User({
        'username': 'nurlan',
        'password': '12345'
    });

    saveUser(admin, nurlan, root).then(() => {
        mongoose.disconnect();
    });
});

async function saveUser(...data) {
    let result;
    for (let item in data) {
        result = savePromise(data[item]);
    }
    await result;
}

function savePromise(data) {
    return new Promise(resolve => {
        data.save().then(() => {
            console.log(data.username);
            resolve(data);
        });
    }, reject => {
        console.error(reject);
    });
}
