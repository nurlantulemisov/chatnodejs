function resolveAfter2Seconds(x) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(x);
        }, 2000);
    });
}
function resolveAfter1Seconds(x) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(x);
        }, 1000);
    });
}

async function add2(x) {
    const a = resolveAfter2Seconds(20);
    const b = resolveAfter1Seconds(30);
    return x + await a + await b;
}

add2(10).then(v => {
    console.log(v);  // prints 60 after 2 seconds.
});