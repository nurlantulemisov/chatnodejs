const Socket = require('../lib/Socket').Socket;

class SocketActions extends Socket {
    constructor(io) {
        super(io);
    }
    get events() {
        return {
            'message': 'messageSocket'
        }
    }

    messageSocket(path, data, callback) {
        this.socket.broadcast.emit('done_message', data);
        callback(data);
    }
}

exports.SocketActions = SocketActions;